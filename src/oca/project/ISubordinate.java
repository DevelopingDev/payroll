package oca.project;

/**
 *
 * Interface to be implemented by all people working at the company 
 * who have a manager
 */
public interface ISubordinate {
    
    //getter and setter methods for pay period   
    TimePeriods getTimePeriod();
    
    void setTimePeriod(TimePeriods timePeriod);
    
}
