package oca.project;

/**
 *
 * Enumeration to store Pay Periods available 
 */

public enum Positions {
    AdministrationManager, BranchManager, CEO, FinancialAdministrator, HROfficer, ProgramDeveloper, ProjectManager, Receptionist, SalesConsultant, SalesManager, SeniorProgramDeveloper, SystemAdministrator, SystemAnalyst, Tester 
}
