/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.ArrayList;
import java.util.Date;
import oca.project.IManager;
import oca.project.ISalariedPerson;
import oca.project.ISubordinate;
import oca.project.Positions;
import oca.project.TimePeriods;

/**
 *
 * @author John Wang
 */
public class HROfficer extends Person implements ISalariedPerson, IManager, ISubordinate {

    protected SalaryInformation salaryInformation;
    protected ArrayList<Person> subordinateList;
    private double allocatedBonus;

    public HROfficer(String firstName, String lastName, Date dob, Date startDate, TimePeriods salaryPayPeriod, double baseSalary) {
        super(firstName, lastName, dob, startDate, salaryPayPeriod, Positions.HROfficer, false);
        salaryInformation = new SalaryInformation(baseSalary, startDate);
        subordinateList = new ArrayList<>();
        salaryInformation.CalculateIncrease(baseSalary, startDate);
        setAllocatedBonus(5000);
        //System.out.println(currentSalary);
    }

    @Override
    public TimePeriods getTimePeriod() {
        return super.getSalaryPeriod();
    }

    @Override
    public void setTimePeriod(TimePeriods timePeriod) {
        super.setSalaryPeriod(timePeriod);
    }

    @Override
    public double getCurrentBonus() {
        return salaryInformation.getBonus();
    }

    @Override
    public void setCurrentBonus(double currentBonus) {
        salaryInformation.setBonus(currentBonus);
    }

    @Override
    public ArrayList<Person> getSubordinateList() {
        return this.subordinateList;
    }

    @Override
    public double getBaseSalary() {
        return salaryInformation.getBaseSalary();
    }

    @Override
    public void addSubordinateList(Person subordinate) {
        if (subordinate instanceof ISalariedPerson) {
            this.subordinateList.add(subordinate);
        }
    }

    @Override
    public int getSubordinateListSize() {
        return this.subordinateList.size();
    }

    public double getCurrentSalary() {
        return salaryInformation.getBaseSalary();
    }

    @Override
    public void setAllocatedBonus(double allocatedBonus) {
        this.allocatedBonus = allocatedBonus;
    }

    @Override
    public double getAllocatedBonus() {
        return this.allocatedBonus;
    }

}
