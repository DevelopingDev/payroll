/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.Date;
import oca.project.IContractor;
import oca.project.ISubordinate;
import oca.project.Positions;
import oca.project.TimePeriods;

public class SalesConsultant extends Person implements ISubordinate, IContractor{

    double baseSalary;
    
    public SalesConsultant(String firstName, String lastName, Date dob, Date startDate, double baseSalary) {
        super(firstName, lastName, dob, startDate, CONTRACT_SALARY_PERIOD, Positions.SalesConsultant, true);
        this.baseSalary = baseSalary;
    }
    
    @Override
    public double getBaseSalary() {
        return baseSalary;
    }

    @Override
    public void setTimePeriod(TimePeriods timePeriod) {
        //ensures salary periods can only be set to monthly regardless of what entry provided
        super.setSalaryPeriod(CONTRACT_SALARY_PERIOD);
    }

    // Calculates total monthly payment for contractors by receiving a starting date of salary payment
    // and totalHoursWorked in that month

    @Override
    public double CalculatePay(Date startDateOfPayPeriod, double totalHoursWorked){
        double hourlyRate = this.getBaseSalary();
        
        double pay = hourlyRate * totalHoursWorked;
        
        // Add to monthly payment list
        
        return pay;
    }

    @Override
    public TimePeriods getTimePeriod() {
        return super.getSalaryPeriod();
    }
    
    @Override
    public String toString(){
       return super.toString();
    }
}
