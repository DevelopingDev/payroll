/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.Date;
import oca.project.Positions;
import oca.project.TimePeriods;

/**
 *
 * @author JW
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private Date dob;
    private Date startDate;
    private TimePeriods salaryPeriod;
    private Positions position;
    
    private Boolean isContractor;

    public Boolean isIsContractor() {
        return isContractor;
    }

    public void setIsContractor(Boolean isContractor) {
        this.isContractor = isContractor;
    }

    private SalaryInformation salaryInformation;
    
    //SimpleDateFormat df = new SimpleDateFormat ("dd/MM/yyyy");

    public Person(String firstName, String lastName, Date dob, Date startDate, TimePeriods salaryPeriod, Positions position, boolean isContractor) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.startDate = startDate;
        this.salaryPeriod = salaryPeriod;
        this.position = position;
        this.isContractor = isContractor;
    }
    
    
    @Override
    public String toString(){
        return getFirstName() + " " + getLastName();  
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Positions getPosition() {
        return position;
    }

    public void setPosition(Positions position) {
        this.position = position;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public TimePeriods getSalaryPeriod() {
        return salaryPeriod;
    }

    public void setSalaryPeriod(TimePeriods salaryPeriod) {
        this.salaryPeriod = salaryPeriod;
    }
    
    public double getBaseSalary() {
        return salaryInformation.getBaseSalary();
    }
}
