/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.ArrayList;
import java.util.Date;
import oca.project.IManager;
import oca.project.ISalariedPerson;
import oca.project.Positions;
import oca.project.TimePeriods;

/**
 *
 * @author JW
 */
public class CEO extends Person implements IManager, ISalariedPerson {
    
    //double baseSalary;
    protected SalaryInformation salaryInformation;
    protected TimePeriods timePeriod;
    protected ArrayList<Person> subordinateList;
    private double allocatedBonus;

    public CEO(String firstName, String lastName, Date dob, Date startDate, TimePeriods salaryPeriod, double baseSalary) {
        super(firstName, lastName, dob, startDate, salaryPeriod, Positions.CEO, false);
        salaryInformation = new SalaryInformation(baseSalary, startDate);
        subordinateList = new ArrayList<>();
        salaryInformation.CalculateIncrease(baseSalary, startDate);
        setAllocatedBonus(5000);
    }
    
    @Override
    public ArrayList<Person> getSubordinateList() {
        return this.subordinateList;
    }

    @Override
    public void addSubordinateList(Person subordinate) {
        if (subordinate instanceof ISalariedPerson) {
            this.subordinateList.add(subordinate);
        }
    }

    @Override
    public TimePeriods getTimePeriod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTimePeriod(TimePeriods timePeriod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public double getBaseSalary() {
        return salaryInformation.getBaseSalary();
    }

    @Override
    public int getSubordinateListSize() {
        return this.subordinateList.size();
    }

    public double getCurrentSalary() {
        return salaryInformation.getCurrentBonus();
    }

    @Override
    public double getCurrentBonus() {
        return salaryInformation.getBonus();
    }

    @Override
    public void setCurrentBonus(double currentBonus) {
        salaryInformation.setBonus(currentBonus);
    }

    @Override
    public void setAllocatedBonus(double allocatedBonus) {
        this.allocatedBonus = allocatedBonus;
    }

    @Override
    public double getAllocatedBonus() {
        return this.allocatedBonus;
    }
}
