/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oca.project.model;

import java.util.Date;

/**
 *
 * @author JW
 */
public class BonusReportItem {

    
    // A combined first and last name should be handled before this class
    private String name;
    
    private double bonus;
    
    private Date bonusPaymentDate;

    public BonusReportItem(String name, double bonus, Date bonusPaymentDate) {
        this.name = name;
        this.bonus = bonus;
        this.bonusPaymentDate = bonusPaymentDate;
    }

    public Date getbonusPaymentDate() {
        return bonusPaymentDate;
    }

    public double getBonus() {
        return bonus;
    }
    
    public String getName() {
        return name;
    }
    
    // Output a bonus item's string in a report output format
    @Override
    public String toString(){
        return this.getName() + " has a bonus payment of " + this.getBonus();
    }

    /*
    //List all payments
    
    public String ListPayments(ArrayList<PayrollReportItem> PayrollReportItemList, Date firstDate, Date secondDate){
        //string manipulation of the end result to be on Payroll report form
        // Call outputFormat()
        return "";
    }
    
    //List all bonus payments
    
    public String ListAllBonusPayments(ArrayList<PayrollReportItem> BonusReportItemList){
        //string manipulation of the end result to be on Payroll report form
        // Call ReportItemOutputFormater()
        String report = "";
        
        if (BonusReportItemList.size()>0) {
            for (PayrollReportItem item : BonusReportItemList) {
                report += ReportItemOutputFormater(item, ReportType.Bonus);
            }   
        } else {
             report = "\nNo result to display";;
        }
        return report;
    }
    
    //List all monthly or fornightly payments depends on what parameter get passed in
    public String ListPayments(ArrayList<PayrollReportItem> PayrollReportItems, TimePeriods paymentType){
        //string manipulation of the end result to be on Payroll report form
        // Call outputFormat()
        return "";
    }
    
    public String ReportItemOutputFormater(PayrollReportItem item, ReportType type){
        String result = "";
        
        switch (type) {
            case Salary:
                result = "\n" + item.getName() + " was paid $" + item.getSalary() + " on the " + item.getPayTimePeriod() + ". Payment Period: " + item.getPayPeriod();
                break;
            case Bonus:
                result = "\n" + item.getName() + " has a bonus of " + item.getSalary();
                break;    
            //Never reach condition where report is empty so no need to implement a default section
        }
        
        return result;
    }
    */
    
}
