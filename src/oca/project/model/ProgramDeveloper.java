/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.Date;
import oca.project.ISalariedPerson;
import oca.project.ISubordinate;
import oca.project.Positions;
import oca.project.TimePeriods;

/**
 *
 * @author John Wang
 */
public class ProgramDeveloper extends Person implements ISalariedPerson, ISubordinate {

    protected SalaryInformation salaryInformation;

    public ProgramDeveloper(String firstName, String lastName, Date dob, Date startDate, TimePeriods salaryPayPeriod, double baseSalary) {
        super(firstName, lastName, dob, startDate, salaryPayPeriod, Positions.ProgramDeveloper, false);
        salaryInformation = new SalaryInformation(baseSalary, startDate);
        salaryInformation.CalculateIncrease(baseSalary, startDate);
        //System.out.println(currentSalary);
    }

    @Override
    public TimePeriods getTimePeriod() {
        return super.getSalaryPeriod();
    }

    @Override
    public void setTimePeriod(TimePeriods timePeriod) {
        super.setSalaryPeriod(timePeriod);
    }

    @Override
    public double getBaseSalary() {
        return salaryInformation.getBaseSalary();
    }

    public double getCurrentSalary() {
        return salaryInformation.getBaseSalary();
    }
}
