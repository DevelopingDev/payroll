/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project.model;

import java.util.Date;
import oca.project.TimePeriods;

/**
 *
 * @author John Wang
 */
public class PayrollReportItem {
    /*
    // Default day for when payment calculation is started
    private static final Date PAYMENT_STARTING_DATE = new Date(115, 0, 1);
    
    // Initial Monthly payroll date
    private Date latestMonthlyPayrollDate = PAYMENT_STARTING_DATE;
    
    // Initial fortnightly payroll date
    private Date latestFortnightlyPayrollDate = PAYMENT_STARTING_DATE;
    
    // arraylist should be created on loading
    ArrayList<PayrollReportItem> PayrollReportItems = new ArrayList<>();
    
    ArrayList<PayrollReportItem> BonusReportItems = new ArrayList<>();
    */
    
    // A combined first and last name should be handled before this class
    private String name;
    
    private double salary;
    
    private Date payDate;
    
    private TimePeriods payPeriod;

    public PayrollReportItem(String name, double salary, Date payDate, TimePeriods payPeriod) {
        this.name = name;
        this.salary = salary;
        this.payDate = payDate;
        this.payPeriod = payPeriod;
    }
    
    public TimePeriods getPayPeriod() {
        return payPeriod;
    }

    public Date getPayDate() {
        return payDate;
    }
    
    public double getSalary() {
        return salary;
    }
    
    public String getName() {
        return name;
    }
        
    @Override
    public String toString(){
        return "\n" + this.getName() + " was paid $" + this.getSalary() + " on the " + this.getPayDate() + ". Payment Period: " + this.getPayPeriod();
    }
    
    /*
    public Date getCurrentPayrollDate() {
        return latestMonthlyPayrollDate;
    }
    
    public Date getCurrentFortnightlyPayrollDate() {
        return latestFortnightlyPayrollDate;
    }

    
    //List all payments
    
    public String ListPayments(ArrayList<PayrollReportItem> PayrollReportItemList, Date firstDate, Date secondDate){
        //string manipulation of the end result to be on Payroll report form
        // Call outputFormat()
        return "";
    }
    
    //List all bonus payments
    
    public String ListAllBonusPayments(ArrayList<PayrollReportItem> BonusReportItemList){
        //string manipulation of the end result to be on Payroll report form
        // Call ReportItemOutputFormater()
        String report = "";
        
        if (BonusReportItemList.size()>0) {
            for (PayrollReportItem item : BonusReportItemList) {
                report += ReportItemOutputFormater(item, ReportType.Bonus);
            }   
        } else {
             report = "\nNo result to display";;
        }
        return report;
    }
    
    //List all monthly or fornightly payments depends on what parameter get passed in
    public String ListPayments(ArrayList<PayrollReportItem> PayrollReportItems, TimePeriods paymentType){
        //string manipulation of the end result to be on Payroll report form
        // Call outputFormat()
        return "";
    }
    
    public String ReportItemOutputFormater(PayrollReportItem item, ReportType type){
        String result = "";
        
        switch (type) {
            case Salary:
                result = "\n" + item.getName() + " was paid $" + item.getSalary() + " on the " + item.getPayTimePeriod() + ". Payment Period: " + item.getPayPeriod();
                break;
            case Bonus:
                result = "\n" + item.getName() + " has a bonus of " + item.getSalary();
                break;    
            //Never reach condition where report is empty so no need to implement a default section
        }
        
        return result;
    }
    
    */
}
