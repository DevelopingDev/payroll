/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oca.project.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author John Wang
 */
public class SalaryInformation {
    // Constant double field that contains the rate of biannually salary increment
    public static final double RATE_OF_INCREASE = 1.02;
    private double baseSalary;
    private double currentSalary;
    private Date startDate;
    private double bonus;
    private double currentBonus;
    
    //Decimal formatter req
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    
    // Below we create a formatter with a pattern of #0.00. The # symbol
    // means any number but leading zero will not be displayed. The 0 
    // symbol will display the remaining digit and will display as zero
    // if no digit is available.
    //DecimalFormat formatter = new DecimalFormat("#0.00");

    // Creates a SalaryInformation object with a baseSalary
    public SalaryInformation(double baseSalary, Date startDate) {
        this.baseSalary = baseSalary;
        this.startDate = startDate;
    }

    public double getCurrentSalary() {
        return CalculateCurrentlySalary();
    }
    
    public double getCurrentBonus() {
        return currentBonus;
    }

    public void setCurrentBonus(double currentBonus) {
        this.currentBonus = currentBonus;
    }

    public double getBaseSalary() {
        return this.baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }
    
    // Calculates number of years based on two dates
    public int CalculateNumberOfYears(Date dateStarted, Date dateEnd){
        int startMonth = dateStarted.getMonth();
        int startYear = dateStarted.getYear();
        
        int endMonth = dateEnd.getMonth();
        int endYear = dateEnd.getYear();
        
        int result = endYear - startYear;

        if (startMonth > endMonth) {
            result--;
        } else if (startMonth == endMonth) {
            int startDay = dateStarted.getDate();
            int endDay = dateEnd.getDate();

            if (startDay > endDay) {
                result--;
            }
        }
        return result;
    }
    
    // Calculate 2% increase every two years based on base salary and date employee started working
    public double CalculateIncrease(double baseSalary, Date dateStarted){
        
        int yearsWorked = CalculateNumberOfYears(dateStarted, new Date());
        currentSalary = baseSalary;
        while(yearsWorked >= 2){
            currentSalary *= RATE_OF_INCREASE;
            yearsWorked -= 2;
        }
        return currentSalary;
    }
    
    
    // Calculate the final yearly pay based on the potiential salary increase
    private double CalculateCurrentlySalary(){
        return CalculateIncrease(baseSalary, getStartDate());
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
