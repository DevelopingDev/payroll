/*
 * Utility class
 */
package oca.project.model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import oca.project.IContractor;
import oca.project.IManager;
import oca.project.TimePeriods;

/**
 *
 * @author JW
 */
public class TransactionManager {
    
    //TODO: Go to calling class?
    public static final Date PAYMENT_STARTING_DATE = new Date(115, 0, 1);
    
    //TODO: Go to calling class?
    public Date currentPayrollDate = null;
    
    // Find bonus for selected person
    public String checkCurrentBonus(Person person, ArrayList<BonusReportItem> bonusReportItems){
        // Check existing bonus entry for the current year
            String currentBonus = "";
            DecimalFormat decimalFormatter = new DecimalFormat("$###,###.##");
            for (BonusReportItem bonusReportItem : bonusReportItems) {
                
                //TODO: for testing purpose
                System.out.println("Checking bonus entry name: " + bonusReportItem.getName() + " against subordinate name: " + person.toString());
                
                if (bonusReportItem.getName().equals(person.toString())) {
                    currentBonus = decimalFormatter.format(bonusReportItem.getBonus());
                } else {
                    currentBonus = "No current bonus";
                }
            }
        return currentBonus;
    }
    
    // Assign bonus a list of subordinates from a selected manager
    // TODO: Add new BonusReportItem to BonusReportItems (list) -> let conttroller handle that
    // TODO: Pop up box for warning messages
    public boolean AssignBonusCheck(IManager manager, Person bonusAssignmentPerson, ArrayList<BonusReportItem> bonusReportItems, double bonus){
        
        boolean canAssign = false;
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy");
        DecimalFormat decimalFormatter = new DecimalFormat("$###,###.##");
        String personName = "";
        String result = "";
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        double currentAssignedBonusTotal = 0;
        int bonusEntryYear = 0;
        
        // New array list for manager's subordinates
        ArrayList<Person> subordinateList = manager.getSubordinateList();
        
        // Check for existing bonus entry for the current year
        for (Person person : subordinateList) {
            personName = person.toString();
            for (BonusReportItem bonusReportItem : bonusReportItems) {

                bonusEntryYear = Integer.parseInt(dateFormater.format(bonusReportItem.getbonusPaymentDate()));
                String bonusEntryName = bonusReportItem.getName();

                //TODO: for testing purpose
                System.out.println("Checking bonus entry name: " + bonusReportItem.getName() + " against subordinate name: " + personName);
                System.out.println("Checking bonus entry year: " + bonusEntryYear + " against current year: " + currentYear);

                if (bonusEntryYear == currentYear) {
                    if (bonusEntryName.equals(personName)) {
                        result = personName + " has already got a bonus assigned";
                        JOptionPane.showMessageDialog(null, result, "Announcement", JOptionPane.INFORMATION_MESSAGE);
    
                        canAssign = false;
                        break;
                    }
                }
            }
        }

        // Calculate sum of total bonus spent
        if (canAssign == true) {

            for (Person person : subordinateList) {
                for (BonusReportItem bonusReportItem : bonusReportItems) {
                    bonusEntryYear = Integer.parseInt(dateFormater.format(bonusReportItem.getbonusPaymentDate()));
                    if (bonusEntryYear == currentYear) {
                        currentAssignedBonusTotal += bonusReportItem.getBonus();
                    }
                }
            }

            // Validate the bonus (form-end)
            // Invalid entry check
            // subtract bonus budget from the manager
            if ((manager.getCurrentBonus() - currentAssignedBonusTotal) >= 0) {
                manager.setCurrentBonus(manager.getCurrentBonus() - currentAssignedBonusTotal);
                canAssign = true;
                //result = "The bonus of $" + decimalFormatter.format(bonus) + " has been assigned to " + bonusAssignmentPerson.toString();
            } else {
                
                canAssign = false;
                result = "Manger " + manager.toString() + " cannot allocate more than $" + (5000-currentAssignedBonusTotal);
                JOptionPane.showMessageDialog(null, result, "Announcement", JOptionPane.INFORMATION_MESSAGE);
    
            }
        }
        // return either error message or amount assigned
        return canAssign;
    }
    
    // Create a payroll report item from payroll input details
    
    // TODO: In the calling class
    
        //Check if worker is on contract or salary
    
        // Gets current fortnightly date
        // Add 2 weeks to current monthly date
        // Sets current fortnightly date
        // iterate throught employee list
            // Check if it's a fornightly pay period
            // Check if it's NOT a IContractor type (additional business logic checking)
            // Call Add AddPayrollReportItems to Payroll report item to array
    
    // Gets current monthly date
        // Add 1 month to current monthly date
        // Sets current monthly date
        // iterate throught employee list
            // Check if it's a monthly pay period
            
            // Call Add AddPayrollReportItems to Payroll report item to array
            
    
    public PayrollReportItem constructSalariedPayment(String name, double baseSalary, Date payDate, TimePeriods payPeriod){

        PayrollReportItem payItem = null;
        
        if (payPeriod == TimePeriods.FORTNIGHTLY) {
            
            // curent base baseSalary / 52 * 2 = fornightly payment
            double fortnightlySalary = baseSalary / 52 * 2;
           // Create fortnightly pay item to be pass back to caller
            payItem = new PayrollReportItem(name, fortnightlySalary, payDate, TimePeriods.FORTNIGHTLY);
            
        } else {
            
            // curent base baseSalary / 12 = monthly payment
            double monthlySalary = baseSalary / 12;
            // Create monthly pay item to be pass back to caller
            payItem = new PayrollReportItem(name, monthlySalary, payDate, TimePeriods.MONTHLY);
            
        }
        return payItem;
    }    
    
    public PayrollReportItem constructContractPayment(IContractor contractor, double totalHoursWorked, Date payDate){
        
        // Initialise PayrollReportItem
        PayrollReportItem payItem = null;
        // curent base baseSalary / 52 * 2 = fornightly payment
        double monthlySalary = contractor.CalculatePay(payDate, totalHoursWorked);
        // Create monthly pay item to be pass back to caller
        return payItem = new PayrollReportItem(contractor.toString(), monthlySalary, payDate, TimePeriods.MONTHLY);
    }

}
