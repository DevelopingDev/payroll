package oca.project;

import java.util.ArrayList;
import oca.project.model.Person;

/**
 *
 * Interface that all the managers should implements
 */
public interface IManager extends IPaidPerson {
    
    ArrayList<Person> getSubordinateList();
    void addSubordinateList(Person subordinate);
    int getSubordinateListSize();
    
    //getter and sett of methods for bonuses 
    double getCurrentBonus();
    
    void setCurrentBonus(double currentBonus);
    
    //getter and sett of methods for allocated Bonuses that might be assigned 
    //to subordinate
    void setAllocatedBonus(double allocatedBonus);
    
    double getAllocatedBonus();
}
