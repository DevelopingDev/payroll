/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project;

import java.util.ArrayList;
import java.util.Date;
import oca.project.model.*;

/**
 *
 * @author JW
 */
public interface ICalculatePayment {
    
    public static final Date PAYMENT_STARTING_DATE = new Date(115, 0, 1);
    
    public Date currentPayrollDate = null;
    
    ArrayList<PayrollReportItem> PayrollReportItems = new ArrayList<>();
    ArrayList<PayrollReportItem> BonusReportItems = new ArrayList<>();
    
    void AddBonusPayments(ArrayList<Person> employeeList);
    
    void AddFornightlyPayments(ArrayList<Person> employeeList);
    
    void AddMonthlyPayments(ArrayList<Person> employeeList);
    
    void AddContractWorkerMonthlyPayment (IContractor contractor);
    
    boolean CheckContractPaymentHistory(Date paymentDate);
    
    //add item to PayrollReportItems
    void AddPayrollReportItems(String name, double salary, String payTimePeriod, TimePeriods payPeriod);
    
    //add item to BonusReportItems
    
    void AddBonusReportItems(String name, double bonus);
    
    //List all payments
    
    String ListPayments(ArrayList<PayrollReportItem> PayrollReportItems, Date firstDate, Date secondDate);
    
    //List all bonus payments
    
    String ListAllBonusPayments();
    
    //List all monthly or fornightly payments depends on what parameter get passed in
    String ListPayments(ArrayList<PayrollReportItem> PayrollReportItems, TimePeriods paymentType);
    
    
}
