package oca.project;

import java.util.Date;

/**
 *
 * Interface to be implemented by all people working at the company 
 */
public interface IPaidPerson{
    
    //getter methods for base salary
    double getBaseSalary();
    
    //this method should be used by all paid people
    //to calculate the pay earned over a period of time
   // public double CalculatePay(Date startDateOfPayPeriod, double totalHoursWorked) throws Exception ;
    
    
}
