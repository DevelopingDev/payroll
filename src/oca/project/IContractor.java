/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.project;

import java.util.Date;

/**
 *
 * @author JW
 */
public interface IContractor extends IPaidPerson{
    // create and initialise base salary
    double baseSalary = 0;
    
    // Full time hour per week is 40
    public static final double FULL_TIME_HOURS = 40;
    
    // Monthly salary payment period
    public static final TimePeriods CONTRACT_SALARY_PERIOD = TimePeriods.MONTHLY;
    
    //
    public double CalculatePay(Date startDateOfPayPeriod, double totalHoursWorked);

    // Output a bonus item's string in a report output format
    
    @Override
    public String toString();

}
