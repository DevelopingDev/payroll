/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oca.test;

import oca.project.model.Person;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JW
 */
public class CanOutputPersonName {
    /*
    public CanOutputPersonName() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    
    @After
    public void tearDown() {
    }
    */
    
    // For resuage through out the tests
    private Person person;
    
    @Before
    public void setUp() {
//        person = new Person();
//        person.setFirstName("John");
//        person.setLastName("Wang");
    }
    
    @After
    public void cleaUp() {
        // delete temporary created objects
    }
    
    @Test
    public void canAccuratelyDisplayName() {
        // Arrange
        //UnitConverter uc = new UnitConverter(); // this line can be refactored into a private variable and have the instantiation in the @Before file
        
        String expected = "John Wang ";
        // Act
        String actual = person.toString();
        // Assert
        assertEquals("Inaccuracy in toString override function",expected, actual);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
